FROM nginx:stable
WORKDIR /usr/share/nginx/html
COPY ./index.html .
COPY ./images/ ./images/
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
