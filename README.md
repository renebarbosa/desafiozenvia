# DESAFIO ZENVIA

Para realizar este desafio, converti o meu CV de PDF para HTML usando um site na Internet e criei o Dockerfile para copiar os arquivos ao DocRoot 
da imagem Docker pública do Nginx.

Como plugins no Kubernetes, adicionei o cert-manager para emissão e gerenciamento dos certificados SSL e o nginx-ingress como controlador de entrada e
proxy reverso no meu cluster (rodando num Azure Kubernetes Service):

Segui os passos abaixo:

## Instalando o cert-manager


```
$ helm repo add jetstack https://charts.jetstack.io
$ helm repo update
$ kubectl create ns cert-manager
$ helm install \
    cert-manager jetstack/cert-manager \
    --namespace cert-manager \
    --version v1.0.4 \
    --set installCRDs=true
$ kubectl create -f letsencrypt.yaml
```

## Instalando o Ingress Controller


```
$ helm repo add stable https://charts.helm.sh/stable
$ helm repo update
$ kubectl create ns ingress 
$ helm install \
    ingress stable/nginx-ingress \
    --namespace ingress
```

## Criando os recursos do Desafio


1- Namespace:

```
$ kubectl create ns desafiozenvia
```

2- Deployment:

```
$ kubectl create -f desafiozenvia_deploy.yaml
```

3- Service:

```
$ kubectl create -f desafiozenvia_svc.yaml
(Também poderia ser criado com kubectl -n desafiozenvia expose deploy desafiozenvia)
```

4- Ingress:

```
$ kubectl create -f desafiozenvia_ingress.yaml
```

O serviço estará disponível em **https://renebarbosa.com.br/** após alguns instantes.